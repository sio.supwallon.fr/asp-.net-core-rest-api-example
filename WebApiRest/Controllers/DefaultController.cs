﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Text.Json;

namespace WebApiRest.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class DefaultController : ControllerBase
    {
        // GET: api/Default
        [HttpGet]
        public IEnumerable<string> Get()
        {
            Console.WriteLine("HTTP Request GET");
            return new string[] { "value1", "value2" };
        }

        // GET: api/Default/5
        [HttpGet("{id}", Name = "Get")]
        public string Get(int id)
        {
            Console.WriteLine("HTTP Request GET id ({0})", id);
            return "value";
        }

        // POST: api/Default
        [HttpPost]
        public void Post([FromBody] JsonElement json)
        {
            Console.WriteLine("HTTP Request POST id ({0})", json.GetProperty("id").GetString());
        }

        // PUT: api/Default/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] JsonElement json)
        {
            Console.WriteLine("HTTP Request PUT id ({0}) value ({1})", id, json.GetProperty("value").GetString());
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            Console.WriteLine("HTTP Request DELETE id ({0})", id);
        }
    }
}
